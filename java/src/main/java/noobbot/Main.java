package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.function.BinaryOperator;

import com.google.common.collect.Lists;
import com.google.common.primitives.Floats;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Main {

    private float throttle;
    private int currentPiece;
    private float[] cornerWeights;
    private float[] desiredSide;
    private float[][] desiredPowerReduction;
    private MyCarData myCar;
    private int gameTick;
    private float velocity;
    private float currentDistance;
    private float currentAngle;
    private Stack<Float> angularChangeHistory;
    private Track track;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        currentPiece = -1;
        currentDistance = 0f;
        velocity = 0f;
        throttle = 1f;
        currentAngle = 0f;
        angularChangeHistory = new SizedStack<Float>(10);

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = new Gson().fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                tick(line);
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                System.out.println("Car");
                myCar = parseMyCar(line).data;
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                Race race = parseRace(line).data.race;
                track = race.track;
                cornerWeights = cornerWeights(race.track.pieces);
                desiredSide = desiredSide(race.track.pieces);
                desiredPowerReduction = new float[][]{
                        desiredPowerReduction(race.track.pieces,1),
                        desiredPowerReduction(race.track.pieces,2),
                        desiredPowerReduction(race.track.pieces,3),
                        desiredPowerReduction(race.track.pieces,4),
                        desiredPowerReduction(race.track.pieces,5),
                        desiredPowerReduction(race.track.pieces,6),
                        desiredPowerReduction(race.track.pieces,7),
                        desiredPowerReduction(race.track.pieces,8),
                        desiredPowerReduction(race.track.pieces,9)
                };
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("Crashed");
            } else {
                send(new Ping());
            }
        }
    }

    private void tick(String line) {
        CarPositionsMessage carPositions = parseCarPositions(line);
        CarPosition myCarPosition = myCarPosition(carPositions.data,myCar);

        if(myCarPosition.piecePosition.pieceIndex != currentPiece){
            currentPiece = myCarPosition.piecePosition.pieceIndex;
            if(desiredSide[(currentPiece+1)%desiredSide.length]!=0f){
                String switchTo = desiredSide[(currentPiece+1)%desiredSide.length]>0f?"Right":"Left";
                System.out.println("Switching to the " + switchTo);
                send(new SwitchLane(switchTo));
            }else{
                send(new Throttle(throttle));
            }
        }else {
            velocity = (myCarPosition.piecePosition.inPieceDistance - this.currentDistance)/(carPositions.gameTick-this.gameTick);
            this.angularChangeHistory.push(Math.max((Math.abs(myCarPosition.angle) - Math.abs(this.currentAngle)),0f) / (carPositions.gameTick - this.gameTick));
            send(new Throttle(throttle));
        }

        this.currentDistance = myCarPosition.piecePosition.inPieceDistance;
        this.gameTick = carPositions.gameTick;
        this.currentAngle = myCarPosition.angle;
//        System.out.println(this.angularChangeHistory.stream().reduce((a,b)->a+b).orElse(0f));

        throttle = 1f - (desiredPowerReduction[(int)velocity][currentPiece]/2f);
        float reducedAngularHistory = Math.abs(this.angularChangeHistory.stream().reduce((a,b)->a+b).orElse(0f));
        if(reducedAngularHistory > 17f && track.pieces[currentPiece].angle != 0f){
            System.out.println("Braking!");
            throttle*=0.5f;
        }else if(reducedAngularHistory < 7f && velocity < 7f){
            System.out.println("Boosting!");
            throttle*=Math.min(1.5f,(1f + (0.7f/(reducedAngularHistory+0.01))));
        }
        System.out.println(String.format("Velocity is %s throttle set to %s", velocity, throttle));
    }

    private static CarPosition myCarPosition(CarPosition[] carPositions,MyCarData myCar){
        for(CarPosition carPosition:carPositions){
            if(carPosition.id.color.equals(myCar.color)){
                return carPosition;
            }
        }
        throw new RuntimeException();
    }

    private static RaceMessage parseRace(String line){
        return new Gson().fromJson(line, RaceMessage.class);
    }

    private static MyCarMessage parseMyCar(String line){
        return new Gson().fromJson(line, MyCarMessage.class);
    }

    private static CarPositionsMessage parseCarPositions(String line){
        return new Gson().fromJson(line, CarPositionsMessage.class);
    }

    private static float[] cornerWeights(Piece[] pieces){
        float[] weights = new float[pieces.length];
        for(int i = 0; i < pieces.length;i++){
            weights[i] = pieces[i].radius * pieces[i].angle;
        }
        return weights;
    }

    private float[] desiredSide(Piece[] pieces){
        float[] output = new float[pieces.length];

        for(int i = 0; i < pieces.length;i++){
            int indexOfNextSwitch = indexOfNextSwitch(i,pieces);
            float weight = 0f;
            for(int j = i; j < indexOfNextSwitch;j++){
                weight+=(cornerWeights[j%pieces.length]);
            }
            output[i] = weight;
        }

        return output;
    }

    private static int indexOfNextSwitch(int index,Piece[] pieces){
        for(int i = 0; i < pieces.length;i++){
            Piece piece = pieces[(i+index+1)%pieces.length];
            if(piece.isSwitch){
                return i+index+1;
            }
        }
        return index;
    }

    private float[] desiredPowerReduction(Piece[] pieces, int readAhead){
        float[] weights = new float[pieces.length];
        for(int i = 0; i < pieces.length;i++){
            float weight = 0f;
            for(int j = 0; j < readAhead; j++){
                weight+= ((Math.abs(cornerWeights[(i+j)%pieces.length])));
            }
            weights[i] = weight;
        }

        float max = Floats.max(weights);

        for(int i = 0; i < pieces.length;i++){
            weights[i] = (weights[i]/max);
        }

        return weights;
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class RaceMessage{
    String msgType;
    RaceData data;
}

class RaceData {
    Race race;
}

class Race{
    Track track;
}

class Track{
    Piece[] pieces;
}

class Piece{
    float length;
    @SerializedName("switch")
    boolean isSwitch;
    int radius;
    float angle;

    @Override
    public String toString(){
        return String.format("length: %s, switch: %s, radius: %s, angle: %s",length,isSwitch,radius,angle);
    }
}

class CarPositionsMessage{
    CarPosition[] data;
    int gameTick;
}

class CarPosition{
    CarId id;
    PiecePosition piecePosition;
    float angle;
}

class CarId{
    String name;
    String color;
}

class PiecePosition{
    int pieceIndex;
    float inPieceDistance;
    Lane lane;
}

class Lane{
    int startLaneIndex;
    int endLaneIndex;
}

class MyCarMessage{
    MyCarData data;
}

class MyCarData{
    String name;
    String color;
}

class SizedStack<T> extends Stack<T> {
    private int maxSize;

    public SizedStack(int size) {
        super();
        this.maxSize = size;
    }

    @Override
    public Object push(Object object) {
        while (this.size() > maxSize) {
            this.remove(0);
        }
        return super.push((T) object);
    }
}